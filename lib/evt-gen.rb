require 'date'

class Identifier
  attr_accessor :root, :root_pad, :prefix, :pre_pad, :suffix, :suf_pad, :delimiter, :size

  def initialize
    @root = ''
    @root_pad = 0
    @prefix = ''
    @pre_pad = 0
    @suffix = ''
    @suf_pad = 0
    @delimiter = '.'
    @size = 0
  end

  def generate(value)
    case
    when ( value.is_a? Range )
      Random.rand value 
    when ( value.is_a? Proc )
      value.call
    else value
    end
  end

  def pad(value, size=0, padding='0')
    if (value.to_s.size < size)
      "#{ padding * (size - value.to_s.size) }#{ value }"
    else value
    end
  end

  def to_s
    "#{ pad (generate @prefix), @pre_pad }#{ @delimiter}#{ pad (generate @root), @root_pad }#{ @delimiter }#{ pad (generate @suffix), @suf_pad }"
  end
end


class Replicator
  def initialize(value)
    @value = value
  end

  def generate(qty, rate)
    result = []
    1.upto (qty - (qty * rate)) do
      result << @value.to_s
    end
    1.upto (qty * rate) do
      result << (result[ Random.rand (result.size - 1) ])
    end
    result
  end
end


class Address
  def initialize(range=100..999, streets=[])
    @range = range
    @streets = streets
    @units = %w( Unit Apt Ste # P.O.Box )
  end

  def generate
    "#{ Random.rand @range } #{ @streets[ Random.rand (@streets.size - 1)] }#{( ( Random.rand(10) % 2 ) == 1 ) ? ' ' + ( @units[ Random.rand ( @units.size - 1 )].to_s + ' ' + Random.rand(1..999).to_s ) : ''}"
  end
  alias :to_s :generate
end


FullAddress = Struct.new :street, :place do
  def to_s
    "#{ street },#{ place }"
  end
end


FullEmail = Struct.new :adjective, :noun, :domain, :tld do
  def to_s
    "#{ adjective }.#{ noun }@#{ domain }.#{ tld }"
  end
end


FullName = Struct.new :first, :middle, :last do
  def to_s
    "#{ first } #{ middle } #{ last }"
  end
end


Payment = Struct.new :weekend, :gross, :net, :balance do
  def to_s
    "#{ weekend },#{ gross },#{ net },#{ balance }"
  end
end


RandList = Struct.new :list do
  def to_s
    list.any.to_s
  end
end


class Random
  class << self
    def dollar(val=100)
      "%0.2f" % ( rand(val) + ( rand(99) / 100.0 ))
    end

    def timestamp
      now = DateTime.now.strftime('%s').to_i
      DateTime.strptime "#{ rand( ( now - (86400 * 180))..now ) }", '%s'
    end
  end
end


class Array
  def any
    self[ Random.rand( self.size - 1 )]
  end
end


class Samples
  dir = "#{ File.dirname __FILE__  }/../samples"
  lists = Dir.entries( dir ).select { |file| file =~ /^[^\.]/ }
  lists.each do |list|
    define_singleton_method list.to_sym do
      load "#{ dir }/#{ list }"
    end
  end

  class << self
    def load(file)
      File.open( file ).readlines.map {|line| line.strip }
    end
  end
end
