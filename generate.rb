require 'digest'
require "#{ File.dirname __FILE__ }/lib/evt-gen.rb"

# Generate SSNs
max = 100000
ssn = Identifier.new
ssn.delimiter = '-'
ssn.prefix = 999
ssn.pre_pad = 3
ssn.root = 1..99
ssn.root_pad = 2
ssn.suffix = 1..9999
ssn.suf_pad = 4
ssn_gen = Replicator.new ssn
ssns = ssn_gen.generate max, 0.25

# Generate phone numbers
phone = Identifier.new
phone.delimiter = '-'
phone.prefix = 1..999
phone.pre_pad = 3
phone.root = 1..999
phone.root_pad = 3
phone.suffix = 1..9999
phone.suf_pad = 4
phone_gen = Replicator.new phone
phones = phone_gen.generate max, 0.20

# Generate dates
date = Identifier.new
date.delimiter = '/'
date.prefix = 1..12
date.root = 1..29
date.suffix = 2020
date_gen = Replicator.new date
dates = date_gen.generate max, 0.3

# Generate addresses
addr = Address.new 1000..9999, Samples.streets
# city = RandList.new Samples.cities
# state = RandList.new Samples.states
# zip = Identifier.new
# zip.delimiter = ''
# zip.prefix = 1..99999
# zip.pre_pad = 5
place = RandList.new Samples.places
full_addr = FullAddress.new addr, place
addr_gen = Replicator.new full_addr
addresses = addr_gen.generate max, 0.15

# Generate emails
e_adj = RandList.new Samples.adjectives
e_nn = RandList.new Samples.nouns
e_dom = RandList.new Samples.domains
e_tld = RandList.new Samples.tlds
full_email = FullEmail.new e_adj, e_nn, e_dom, e_tld
email_gen = Replicator.new full_email
emails = email_gen.generate max, 0.25

# Generate full names
n_first = RandList.new Samples.fnames
n_mid = RandList.new Samples.letters
n_last = RandList.new Samples.lnames
full_name = FullName.new n_first, n_mid, n_last
name_gen = Replicator.new full_name
names = name_gen.generate max, 0.4

# Generate initial set of claim identifier data
ids = []
0.upto ( max - 1 ) do |i|
  ids << "#{ ssns[i] },#{ dates[i] },#{ names[i] },#{ addresses[i] },#{ phones[i] },#{ emails[i] }"
end

# Web logs
src_ip = Identifier.new
src_ip.prefix = 2..255
src_ip.suffix = 2..255
src_ip.root = Proc.new { Random.rand(100..255).to_s + '.' + Random.rand(100..255).to_s }

svc_ip = Identifier.new
svc_ip.prefix = 10
svc_ip.suffix = 2..8
svc_ip.root = Proc.new { Random.rand(100..255).to_s + '.' + Random.rand(100..255).to_s }

hosts = RandList.new %w( websrv00 websrv02 websrv02 websrv02 websrv03 websrv03 websrv07 webexp00 )

# Balance amounts & week ending dates
weekends = %w( 06/26/20 07/03/20 07/10/20 07/17/20 07/24/20 07/31/20 08/07/20 08/14/20 08/21/20 08/28/20 09/04/20 09/11/20 09/18/20 09/25/20)
payout = File.new './payments.csv', 'w'
# payout = STDOUT
bene_range = 100..600

# final output
puts "Writing out payments..."
ids.each do |id|
  weekends.each do | weekend|
    payment = Payment.new
    payment.weekend = weekend
    payment.gross = Random.dollar(bene_range)
    payment.net = "%0.2f" % ( payment.gross[/[0-9\.]+/].to_f * 0.8 )
    payment.balance = "%0.2f" % ( payment.gross[/[0-9\.]+/].to_f * Random.rand(24) + 600.01 )

    partial = "#{ Random.timestamp },#{ src_ip },#{ svc_ip },#{ hosts }"
    payout.puts "#{ id },#{ payment },#{ partial },#{ Digest::MD5.hexdigest partial }"  unless ( Random.rand(1..22) % 7 ) == 0
  end
end
